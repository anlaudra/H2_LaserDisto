H2_LaserDisto
=============

Script controlling the laser measuring the table position in H2 beam line.

**DISCLAIMER:**
I am not the original author of the code (unknown), I just modified it to have a better logging capability and updated it to python3.


Installation
------------

This program **requires python3** and the [`pySerial`](https://pyserial.readthedocs.io/en/latest/index.html) package installed.

```bash
git clone https://gitlab.cern.ch/anlaudra/H2_LaserDisto.git
cd H2_LaserDisto

python3 laser_monitor_dual.py
```

The other files in the package are only there for backup purposes, one only need the [laser_monitor_dual.py](./laser_monitor_dual.py) script.


Logging
-------

By default, the program writes the position on screen as well as in a logfile.
The logfiles are created in a `logs` directory under the location of the script with name `table_position.txt`.
The logfile path/name can be specified by the user by the option `--logfile`.
To suppress logging to file, pass an empty file name.
One logfile should be created per day, rolling at midnight.
Logfiles are kept for 35 days (hardcoded value), then automatically deleted.

Debug information can be printed using the `--debug` flag

Laser measurements are updated about every second (cannot go below).
This can de decreased by passing a value (in seconds) to `--interval`.


Troubleshooting
---------------

- Sometimes the messages get stuck in the input buffer of the serial connection (the code is checking for this), and the program exits.
  Simply start the code again.
- It can happen that the code crashes during initialisation of the `Serial` objects (often due to the vertical laser).
  This can be solved by unplugging and replugging the corresponding USB cable on the Raspberry Pi.

