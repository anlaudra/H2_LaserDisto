#!/usr/bin/env python
import time
import serial

progstart= time.time()

serV = serial.Serial(
        #port='/dev/ttyUSB_V', #Replace ttyS0 with ttyAM0 for Pi1,Pi2,Pi0
        #port='/dev/tty.usbserial-1413130',  #mac
        port='/dev/ttyUSB0',  #test rpi4
        baudrate = 19200,
        parity=serial.PARITY_NONE,
        stopbits=serial.STOPBITS_ONE,
        bytesize=serial.EIGHTBITS,
        timeout=0
)
serH = serial.Serial(
        #port='/dev/ttyUSB_H', #Replace ttyS0 with ttyAM0 for Pi1,Pi2,Pi0
        #port='/dev/tty.usbserial-14134440',  #mac
        port='/dev/ttyUSB1',  #test rpi4
        baudrate = 19200,
        parity=serial.PARITY_NONE,
        stopbits=serial.STOPBITS_ONE,
        bytesize=serial.EIGHTBITS,
        timeout=0
)
counter=0

while 1:
        #serV.close()
        #serH.close()
        #serV.open()
        #serH.open()
	serV.flush()
	serH.flush()
        serV.write('D')
	serH.write('D')
        #time.sleep(0.5)
        counter += 1

        timestart= time.time()

        while serV.in_waiting<=2 or serH.in_waiting<=2:
	    if time.time()-timestart>=3:
	          print time.time()-progstart
	          exit()

        time.sleep(0.1)
	V=serV.read(size=serV.in_waiting)
	H=serH.read(size=serH.in_waiting)
        print V
	print H

        #time.sleep(3)