#!/usr/bin/env python3
#-*- coding: utf-8 -*-

"""
@date September 2021, updated June 2022.
@brief Monitor the table position of H2 TB area in H and V axes.

Allows logging to a time-rotated file.
"""

import sys
from os import mkdir
import os.path
import time
import logging
from logging.handlers import TimedRotatingFileHandler
from argparse import ArgumentParser

import serial

if sys.version_info[0] < 3:
    print("!"*80)
    print("**ERROR**: you seem to be using python 2 while this script has been migrated to Python 3!")
    print("!"*80)

##############################################################################
### Logger configuration
logger = logging.getLogger(__file__)
FORMATTER = logging.Formatter(fmt="%(asctime)s %(levelname)-8s %(message)s")

def try_mkdir(dir_path):
    """Try to make a directory. If already exists, do not crash."""
    try:
        mkdir(dir_path)
    except FileExistsError:  # If crash here, make sure you are using Python3 !!
        logger.debug("Dir '%s' already exists, not creating.", dir_path)
    else:
        logger.debug("Created dir '%s'", dir_path)

def setup_logging(level=logging.INFO, logfile=""):
    """Setup the root logger.

    @param[in] level: logging level
    @param[in] logfile: string or None
        If a non-empty string is provided, the logger will set up a
        TimedRotatingFileHandler with this path.

    The TimedRotatingFileHandler will create one log per day, up to 35 days
    (the files are deleted in cycle after that time).
    """
    logger.setLevel(level)

    console_handler = logging.StreamHandler(sys.stdout)
    console_handler.setFormatter(FORMATTER)
    logger.addHandler(console_handler)

    if logfile:
        logfile = os.path.normpath(logfile)
        logger.debug("Setting up rotating logfile: '%s'", logfile)
        try_mkdir(os.path.dirname(logfile))

        file_handler = TimedRotatingFileHandler(
            logfile, when='midnight', backupCount=35)
        file_handler.setFormatter(FORMATTER)
        logger.addHandler(file_handler)
    else:
        logger.debug("No logfile will be used: only displaying on console.")

### End logger configuration
##############################################################################


def init_device(port):
    "Utility factoring out settings for Serial device init on a given port."
    return serial.Serial(
        port=port,
        baudrate = 19200,
        parity=serial.PARITY_NONE,
        stopbits=serial.STOPBITS_ONE,
        bytesize=serial.EIGHTBITS,
        timeout=0
    )

def process_answer(msg):
    """Process data message from laser.
    'D: 0.643m,0027\r\n' -> return ("0.643", "0027")
    """
    result = msg[3:].rstrip().split(',')
    result[0] = result[0][:-1]
    logger.debug("Processed %r into (%s, %s).", msg, result[0], result[1])
    return result


class LaserMonitoring():
    "Handling the laser measurements of 2 lasers in H2 beamline."
    def __init__(self):
        self._start_time = time.time()

        # self._laserV = init_device(port='/dev/ttyUSB0')  #test rpi4
        # self._laserH = init_device(port='/dev/ttyUSB1')  #test rpi4

        # self._laserV = init_device(port='/dev/tty.usbserial-1413130')   #mac
        # self._laserH = init_device(port='/dev/tty.usbserial-14134440')  #mac

        # Replace ttyS0 with ttyAM0 for Pi1,Pi2,Pi0
        self._laserV = init_device(port='/dev/ttyUSB_V')
        self._laserH = init_device(port='/dev/ttyUSB_H')


    def _take_measurement(self):
        """Take a single measurement from the two connected devices.

        @return: dict[str] -> (str, str),
            key is the name of the laser ('H' or 'V'),
            value is the pair of measurements given for each laser.
            Example: `{'H': ("1.034", "0031"), 'V': ("0.694", "0028")}`
        """
        logger.debug("Starting new iteration.")
        self._laserV.flush()
        self._laserH.flush()
        self._laserV.write(bytes('D', encoding='ascii'))
        self._laserH.write(bytes('D', encoding='ascii'))
        # time.sleep(0.5)

        timestart = time.time()

        # If message is stuck in the input buffer after 3 seconds, exit.
        while self._laserV.in_waiting <= 2 or self._laserH.in_waiting <= 2:
            logger.debug("Waiting for input buffer to empty.")
            time.sleep(0.1)
            if time.time() - timestart >= 3:
                logger.error("Message stucked in the input buffer.")
                logger.error("Exiting now: crash after %.0f s.",
                             time.time() - self._start_time)
                sys.exit()

        answerV = self._laserV.read(size=self._laserV.in_waiting).decode('ascii')
        logger.debug("Answer V: %r.", answerV)
        answerH = self._laserH.read(size=self._laserH.in_waiting).decode('ascii')
        logger.debug("Answer H: %r.", answerH)

        return {'H': process_answer(answerH),
                'V': process_answer(answerV)}

    def loop(self, interval=0):
        "Main loop, can be exited by Ctrl+C."
        try:
            while True:
                measurements = self._take_measurement()
                logger.info("H = %sm (q:%s)   ||   V = %sm (q:%s)",
                            measurements['H'][0], measurements['H'][1],
                            measurements['V'][0], measurements['V'][1])
                time.sleep(interval)
        except KeyboardInterrupt:
            logger.debug("Caught KeyboardInterrupt, exiting gracefully...")
            print()  # New line after ^C character received.
            return


def main(interval=0, logfile="", debug=False):
    setup_logging(
        level=logging.DEBUG if debug else logging.INFO,
        logfile=logfile)
    monitoring = LaserMonitoring()
    monitoring.loop(interval=interval)

###############################################################################
DESCR = "Monitor the table position of H2 TB area in H and V axes."

def parse_args(argv):
    PWD = os.path.dirname(__file__)
    default_logfile = os.path.join(PWD, "logs", "table_position.txt")

    parser = ArgumentParser(description=DESCR)
    parser.add_argument("--interval", type=float, default=0,
        help="Interval in second to update. "
             "Default (0) gives an update rate of ~1 Hz (cannot go below).")

    args_log = parser.add_argument_group("logging")
    args_log.add_argument("--debug", action="store_true",
        help="Turn on debug output.")
    args_log.add_argument("--logfile", default=default_logfile,
        help="Path for the rotating logfile. "
             "Default: " + default_logfile + ". "
             "Pass an empty string to disable.")

    # Return a dictionary of the arguments.
    return vars(parser.parse_args(argv))


if __name__ == "__main__":
    main(**parse_args(sys.argv[1:]))

